use std::fs::File;
use std::io::prelude::*;
use toml;

#[derive(Default, Deserialize)]
pub struct Config {
    pub address: String,
    pub key: String,
    pub base_dir: String,
}

impl Config {
    pub fn clone(&self) -> Config {
        Config {
            address: self.address.clone(),
            key: self.key.clone(),
            base_dir: self.base_dir.clone(),
        }
    }

    pub fn from_toml(filename: String) -> Result<Config, String> {
        let file = File::open(filename.as_str());
        if file.is_err() {
            let err_msg = format!("Failed to read {}", filename.clone());
            return Err(err_msg);
        }
        let mut file = file.unwrap();
        
        let mut content = String::new();
        let result = file.read_to_string(&mut content);
        if result.is_err() {
            return Err(format!("Failed to read into string"));
        }

        let config = toml::from_str(content.as_str());
        if config.is_err() {
            let err_msg = format!("Failed to parse toml: {:?}", config.err());
            return Err(err_msg.to_string());
        }
        
        return Ok(config.unwrap());
    }
}