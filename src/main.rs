extern crate uuid;
extern crate serde_json;
extern crate toml;

#[macro_use]
extern crate serde_derive;
extern crate nickel;

#[macro_use]
extern crate lazy_static;

mod filesys;
mod config;
mod deps;

use nickel::{Nickel, HttpRouter, Request, Response, MiddlewareResult, status::StatusCode, MediaType};

use std::path::Path;
use std::sync::Mutex;

use deps::Deps;

lazy_static! {
    static ref DEPENDENCY: Mutex<Deps> = Mutex::new(Deps::default());
}

fn list_files_in_dir<'mw>(req: &mut Request, mut res: Response<'mw>) -> MiddlewareResult<'mw> {
    let key: String; 

    // Scope needed otherwise it will dead-lock:
    {
        let deps = &DEPENDENCY.lock().unwrap();
        key = deps.config().key.clone();
    }

    let segments: Vec<&str> = req.path_without_query()
                                 .unwrap()
                                 .trim_right_matches('/')
                                 .split('/')
                                 .skip(2)
                                 .collect();

    let path = segments.join("/");

    let fm = create_file_manager(path.to_string());
    let files = fm.list_files(key);
    if files.is_err() {
        return res.error(StatusCode::NotFound, "Path not found");
    }
    let files = files.unwrap();

    res.set(MediaType::Json);
    let json = serde_json::to_string(&files);

    return res.send(json.unwrap());
}

fn serve_file<'mw>(req: &mut Request, mut res: Response<'mw>) -> MiddlewareResult<'mw> {
    let hash = req.param("hash").unwrap();
    let key: String;

    // Scope needed otherwise it will dead-lock:
    {
        let deps = &DEPENDENCY.lock().unwrap();
        key = deps.config().key.clone();
    }

    let segments: Vec<&str> = req.path_without_query()
                                 .unwrap()
                                 .trim_right_matches('/')
                                 .split('/')
                                 .skip(3)
                                 .collect();

    let path = segments.join("/");

    let fm = create_file_manager(path.to_string());

    let file = fm.find_file_by_hash(hash.to_string(), key);
    if file.is_none() {
        return res.error(StatusCode::NotFound, format!("hash {}", hash));
    }

    let file = file.unwrap();
    let path = Path::new(&file.path);

    let value = format!("attachment; filename=\"{}\"", path.file_name().unwrap().to_str().unwrap());

    res.headers_mut().set_raw("Content-Disposition", vec![value.as_bytes().to_vec()]);

    return res.send_file(path);
}

fn create_file_manager(path_str: String) -> filesys::Manager {
    let dep = DEPENDENCY.lock().unwrap();

    let path = Path::new(&dep.config().base_dir);

    if path.to_string_lossy().trim() != String::new() {
        let path = path.join(Path::new(&path_str));
        return filesys::Manager::new(path.to_str().unwrap().to_string());
    }

    return filesys::Manager::new(String::new());
}

fn main() {
    let config = config::Config::from_toml(String::from("config.toml"));
    if config.is_err() {
        let err_msg = format!("{:?}", config.err());
        println!("Failed to read config: {}", err_msg);
        return;
    }
    let config = config.unwrap();

    {
        let mut dep = DEPENDENCY.lock().unwrap();
        dep.configure(config.clone());
            /*Config {
            key: "z87GBVpVfX8p8QMZAru9K".to_string(),
            base_dir: r"C:\Users\andi\AppData\Local\Temp".to_string(),
        }*/
    }

    let mut server = Nickel::new();

    server.get("/list/**", list_files_in_dir);
    server.get("/list", list_files_in_dir);

    server.get("/get/:hash/**", serve_file);
    server.get("/get/:hash", serve_file);

    println!("Directory root: {}", config.base_dir.clone());
    let host = config.address;
    server.listen(host.clone()).expect(&format!("Failed to listen on {}", host));
}