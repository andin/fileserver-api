use std::fs;
use std::io;
use uuid;

#[derive(Serialize, Deserialize)]
pub struct File {
    pub hash: String,
    pub path: String,
}

impl File {
    pub fn clone(&self) -> File {
        return File {
            hash: self.hash.clone(),
            path: self.path.clone(),
        };
    }
}

pub struct Manager {
    base_dir: String
}

impl Manager {
    pub fn new(base_dir: String) -> Manager {
        return Manager {
            base_dir: base_dir,
        };
    }

    pub fn find_file_by_hash(&self, hash: String, key: String) -> Option<File> {
        let files = self.list_files(key);
        if files.is_err() {
           return None; 
        }
        let files = files.unwrap();

        for file in files.iter() {
            if file.hash == hash {
                return Some(file.clone());
            }
        }

        None
    }

    pub fn list_files(&self, key: String) -> Result<Vec<File>, io::Error> {
        let paths = try!(fs::read_dir(self.base_dir.clone()));

        let mut files : Vec<File> = vec![];

        for path in paths {
            if path.is_err() {
                continue;
            }

            let path = path.unwrap();
            let md = fs::metadata(path.path());
            if md.is_err() {
                continue;
            }
            let md = md.unwrap();

            if !md.is_file() {
                continue
            }

            let path = path.path();
            let path_str = path.to_str();
            if !path_str.is_some() {
                continue;
            }
            let path_str = path_str.unwrap();

            let file_id = uuid::Uuid::new_v5(&uuid::NAMESPACE_DNS, 
                           format!("{}{}", path_str, key).as_str());

            let file = File {
                hash: file_id.simple().to_string(),
                path: path_str.to_string()
            };

            files.push(file);
        }

        Ok(files)
    }
}
