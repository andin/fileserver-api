use config::Config;

#[derive(Default)]
pub struct Deps {
    config: Config,
}

impl Deps {
    pub fn configure(&mut self, config: Config) {
        self.config = config;
    }

    pub fn config(&self) -> &Config {
        &self.config
    }
}